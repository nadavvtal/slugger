import { slugger } from "./index.js"
/**
 * @describe slugger - group of tests with a header to describe them
 */
describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {

        expect(slugger("with great power comes great responsibility")).toEqual("with-great-power-comes-great-responsibility");
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        expect(slugger("hi you", "where are", "you going to", "or not")).toEqual("hi-you-where-are-you-going-to-or-not");
    })

    it("should be defined", () => {
        expect(slugger).toBeDefined();
    });

    it("should be a function", () => {
        expect(slugger).toBeInstanceOf(Function);
    });

    it("should throw expect at least one string", () => {
        expect(() => {
            slugger();
        }).toThrowError(`slugger expects at least one string as input`);
    });

    it("slugger expects only strings", () => {
        expect(() => {
            slugger("hi there", "the next one isnt string", 2, "should throw error");
        }).toThrowError(`slugger expects only strings`);
    });

})


