
export function slugger(...strings) {
    if (strings.length === 0) {
        throw new Error("slugger expects at least one string as input")
    }

    let ans = " ";
    const ansArr = strings.map((sentence) => {
        if (typeof sentence !== "string") {
            throw new Error("slugger expects only strings")
        }
        return sentence.split(" ").join("-");
    })
    ansArr.forEach((setence => {
        ans += `-${setence}`;
    }));
    return ans.substring(2);
}
