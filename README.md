# Slugger
## Js function that can turn any blog post title into url friendly slug
### **Inputs** : string or multiple strings that represents sentences which has spaces between the words.
### **Returns**: one string that represent slug of all the input strings. 